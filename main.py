#inspired by https://gitlab.com/a.vosnakis/nakistext

#notes on tkinter
#http://effbot.org/tkinterbook/text.htm
#https://www.tutorialspoint.com/python/tk_text.htm

import tkinter as tk
from tkinter import filedialog
from tkinter.font import Font
import keyword

import sys, os
def blockPrint():
	sys.stdout = open(os.devnull, 'w')

END = tk.END#'end'
INSERT = tk.INSERT#'insert'

HIGHLIGHT_TEXT_TAG = 'HIGHLIGHT_TEXT_TAG'
END_TAG = "endTag"



class Application(tk.Text):
	
	#replace keywords with smaller list to highlight fewer words
	#keywords = {'def':'defTag', 'for':'forTag', 'while':'whileTag', 'if':'ifTag', 'elif':'elifTag', 'else':'elseTag', 'in':'inTag'}
	keywords = dict( (key, key+'Tag') for key in keyword.kwlist )
	
	def __init__(self, parent):
		
		self.selectedContent = ''
		
		tk.Text.__init__(self, parent)
		tk.Text.grid(self, columnspan = 4)
		tk.Text.config(self, width=62)
		tk.Text.config(self, height=34)
			
		#compile button
		compile = tk.Button(parent, text="Compile", height=2, width=18, command=self.compileBtn)
		compile.grid(row=1, column=0)
		
		#TODO button
		self.TODO = tk.Label(parent, text="TODO", height=2, width=18)#, command=self.TODOBtn)
		self.TODO.grid(row=1, column=1)
		
		#save as button
		saveAsButton = tk.Button(parent, text="Save", height=2, width=18, command=self.saveAs)
		saveAsButton.grid(row=2, column=1, padx=6, pady=6)#adding padding to one button in row 2 adds it effectively to all buttons of row 2 because packing of widget
		
		#highlight buttons and text field
		HighlightButton = tk.Button(parent, text="Highlight All", height=2, width=18, command=self.highlightText)
		HighlightButton.grid(row=1, column=2)
		ClearHighlightButton = tk.Button(parent, text="Clear Highlight", height=2, width=18, command=self.clearHighlightText)
		ClearHighlightButton.grid(row=2, column=2)
		self.searchText = tk.Text(parent, height=3, width=30)
		self.searchText.grid(row=1, column=3, rowspan = 2)
		
		
		#TODO interpreter
		self.interpreterText = tk.Text(parent, height=50, width=30)
		self.interpreterText.grid(row=0, column=4, padx=10, pady=5, rowspan = 1)
		
		
		#info label
		self.infoLabel = tk.Label(parent, text="Welcome", height=4, width=28, wraplength=100, foreground='#ff3000')
		self.infoLabel.grid(row=1, column=4, rowspan=2)

		
		#add tags for keywords, highlight and last char
		for keyword, keywordTag in self.keywords.items():
			self.tag_config(keywordTag, foreground="#5022e7")
		
		self.tag_config(HIGHLIGHT_TEXT_TAG, foreground="#ff0000", underline=1)
		self.tag_config(END_TAG, background="#535e02", foreground="#ffb2e7")
		
		
		#TODO add scrollbar
		#sb = tk.Scrollbar(self, orient="vertical", command=text.yview)
		#self.configure(yscrollcommand=sb.set)
		#sb.pack(side="left", fill="y")
		
		
		self.config(font=("Times New Roman", 16))
		
		self.focus_set()
		self.configure(state="disabled")

		font = tk.Menubutton(parent, text="Font")
		font.grid(row=2, column=0)
		font.menu = tk.Menu(font, tearoff=0)
		font["menu"] = font.menu
		helvetica = tk.IntVar()
		courier = tk.IntVar()
		tnr = tk.IntVar()

		font.menu.add_checkbutton(label="Courier", variable=courier, command=self.FontCourier)
		font.menu.add_checkbutton(label="Helvetica", variable=helvetica, command=self.FontHelvetica)
		font.menu.add_checkbutton(label="Times New Roman", variable=tnr, command=self.FontTNR)


		
	def FontHelvetica(self):
		self.config(font=("Helvetica", 15))

	def FontCourier(self):
		self.config(font=("Courier", 14))

	def FontTNR(self):
		self.config(font=("Times New Roman", 16))
		

	def compileBtn(self):
		
		#>>> program = 'a = 5\nb=10\nprint("Sum =", a+b)'
		#>>> exec(program)
		
		print('input:', self.retrieve_input())
		print('output:')
		
		try:
			
			locals = {}
			exec(self.retrieve_input(), globals(), locals)
			#self.infoLabel['text'] = locals
			#https://stackoverflow.com/questions/3906232/python-get-the-print-output-in-an-exec-statement
			
		except Exception as e:
			print('Error:', e)
			self.infoLabel['text'] = e
			
		self.interpreterText.see(END)
		
	
	def saveAs(self):
		"""Script for the save as button to work"""
		myInitialdir = "D:",
		myTitle = "Choose your file"
		myFiletypes = ( ("Python Files", "*.py"), ("Text Files", "*.txt"), ("All Files", "*.*") )
		saveLocation = filedialog.asksaveasfilename(initialdir=myInitialdir, title=myTitle, filetypes=myFiletypes, confirmoverwrite=True)
		if saveLocation != '':
			text = self.retrieve_input()
			file = open(saveLocation+'.py', "w+")
			file.write(text)
			print('saved to:' + saveLocation)
			file.close()

	def retrieve_input(self):
		return self.get("1.0", END+'-1c')
		
	
	def clarAllText(self):

		self.configure(state="normal")
		self.delete('1.0', END)
		self.configure(state="disabled")
		
	
	def backspace(self):
		print('backspace()')

		self.configure(state="normal")
		try:
			self.delete("sel.first", "sel.last")
		except:
			self.delete(INSERT+'-1c')
		self.configure(state="disabled")
		
	
	def deleteForward(self):
		print('backspace()')

		self.configure(state="normal")
		try:
			self.delete("sel.first", "sel.last")
		except:
			self.delete(INSERT)
		self.configure(state="disabled")
		
		
	def tab(self):
		
		self.configure(state="normal")
		#self.insert(INSERT, '    ')#self.set('\t\r')#tab
		#self.configure(state="disabled")
		
		
	def paste(self):
		
		self.configure(state="normal")
		
		self.insert(INSERT, self.selectedContent)
		#TODO add clipboard content instead of self.selectedContent
		#https://stackoverflow.com/questions/101128/how-do-i-read-text-from-the-windows-clipboard-from-python
		
		self.configure(state="disabled")
		
		
	def set(self, what):
		
		self.configure(state="normal")
		self.insert(INSERT, what)
		self.configure(state="disabled")
		
		

	def highlightText(self, text=''):
		
		self.tag_remove(HIGHLIGHT_TEXT_TAG, '0.0', END)
		
		if text == '':
			#get text from field
			text = self.searchText.get("1.0",END+'-1c')
			
		self.highlightPattern(text, HIGHLIGHT_TEXT_TAG)
		
		
	def clearHighlightText(self):
		
		self.tag_remove(HIGHLIGHT_TEXT_TAG, '0.0', END)
		self.searchText.delete('1.0', END)
		
		
	#highlight keywords and END_TAG(point at which the pointer is)
	def highlight(self):
		
		self.tag_remove(END_TAG, '0.0', END)
		self.tag_add(END_TAG, INSERT, INSERT+'+1c')#END+'-1c' for one char syntax highlight
		
		for keyword, keywordTag in self.keywords.items():
			self.tag_remove(keywordTag, '0.0', END)
		
		for keyword, keywordTag in self.keywords.items():
			self.highlightPattern(keyword, keywordTag)
		
		
	def highlightPattern(self, pattern, tag, start="1.0", end="end", regexp=False):

		start = self.index(start)
		end = self.index(end)
		self.mark_set("matchStart", start)
		self.mark_set("matchEnd", start)
		self.mark_set("searchLimit", end)

		count = tk.IntVar()
		while True:
			index = self.search(pattern, "matchEnd", "searchLimit", count=count, regexp=regexp)
			if index == "": break
			if count.get() == 0: break#degenerate pattern which matches zero-length strings
			self.mark_set("matchStart", index)
			self.mark_set("matchEnd", "%s+%sc" % (index, count.get()))
			self.tag_add(tag, "matchStart", "matchEnd")
			

def callback(event):
	print("clicked at", event.x, event.y)
	mainApp.configure(state="normal")
	
	
def keyBefore(event): mainApp.configure(state="disabled")
	
	
def key(event):
	
	print("pressed:", repr(event.char), 'keycode:', event.keycode)#, 'event:', event
	
	if event.keycode == 13: mainApp.set('\n')#enter
	elif event.keycode == 8: mainApp.backspace()#backspace
	elif event.keycode == 46: mainApp.deleteForward()#delete key
	elif event.keycode > 36 and event.keycode < 41: mainApp.configure(state="disabled")#arrows
	elif event.keycode == 9: return#tab
	else: mainApp.set(event.char)
	
	#print('calling heighlight()')
	mainApp.highlight()
	
	#add copy/paste ability
	content = ''
	try:
		mainApp.selectedContent = mainApp.selection_get()
		print('Selected text:', mainApp.selectedContent)
	except:
		print()

	#TODO add character_count = textwidget.count("1.0", "sel.first")
	
	
def tab(event):
	print('tab')
	mainApp.tab()
	
def ctrlShiftLeft(event):
	print('ctrlShiftLeft')
	mainApp.tag_remove(END_TAG, '0.0', END)
	
def ctrlShiftRight(event):
	print('ctrlShiftRight')
	mainApp.tag_remove(END_TAG, '0.0', END)
	
def ctrlV(event):
	print('ctrl+v')
	mainApp.paste()
	
def select_all(event):
	print('select_all')
	selTag = 'selTag'
	mainApp.tag_add(selTag, "1.0", END)
	mainApp.mark_set(INSERT, "1.0")
	mainApp.see(INSERT)
	
	
root = tk.Tk()
mainApp = Application(root)
#TODO add interpreter window (>>>) to mainApp

def main():

	root.title("Pyd 1.0")
	
	'''
	TODO improve:<Key> instead of <KeyRelease> doesn't work here: mainApp.bind("<KeyRelease>", key)
	because x,y coordinates of INSERT don't get updated so END_TAG highlight lags by one arrow press
	solution: https://stackoverflow.com/questions/27535023/why-text-cursor-coordinates-are-not-updated-correctly
	solution2: https://stackoverflow.com/questions/27645460/python-tkinter-text-insert-current-cursor
	because it is on <KeyRelease> it still looks a little laggy, but doesn't actually lag by one whole arrow press
	'''
	mainApp.bind("<Key>", keyBefore)
	mainApp.bind("<KeyRelease>", key)
	mainApp.bind("<Button-1>", callback)
	mainApp.bind("<Tab>", tab)
	mainApp.bind("<Control-Shift-Left>", ctrlShiftLeft)
	mainApp.bind("<Control-Shift-Right>", ctrlShiftRight)
	mainApp.bind("<Control-v>", ctrlV)
	mainApp.bind("<Control-V>", ctrlV)#in case caps lock is on

	#Add the binding to select all the text in textbox
	mainApp.bind("<Control-Key-a>", select_all)
	mainApp.bind("<Control-Key-A>", select_all)#in case caps lock is on
	
	#TODO add Ctrl+Z
	
	mainApp.mainloop()
	
def cutomPrint(*s): #redefining the built-in print function
		
	mainApp.configure(state="normal")
	try:
		for i in s:
			mainApp.interpreterText.insert(END,"\n"+str(i))
			'''
			if type(mainApp.infoLabel.cget("text")) == str and type(i) == str:#problem:i can also be int (i.e. print(5))
				#mainApp.infoLabel['text'] = mainApp.infoLabel.cget("text") + i
				mainApp.interpreterText.insert(END,"\n"+i)
			'''
	except IndexError:
		pass
	mainApp.configure(state="disabled")
	
	
if __name__ == '__main__':
	#blockPrint()
	print = cutomPrint
	main()